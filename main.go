package main

import (
	"gitlab-depscan/config"
	"gitlab-depscan/plugin"
	"os"

	"github.com/urfave/cli"
	"gitlab-depscan/logutils"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"

	log "github.com/sirupsen/logrus"
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "gitlab-depscan analyzer for GitLab Omnibus"
	app.Author = "GitLab"

	log.SetFormatter(&logutils.VulnerabilityFormatter{Project: scannerName})

	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameDependencyScanning,
		Match:        plugin.Match,
		Analyze:      analyze,
		AnalyzeFlags: []cli.Flag{
			&cli.StringFlag{
				Name:   config.DenyListParam,
				EnvVar: config.DenyListEnv,
				Usage:  "Path to denylist file.",
			},
			&cli.StringFlag{
				Name:   config.ManifestParam,
				EnvVar: config.ManifestEnv,
				Usage:  "filename of manifest file.",
			},
			&cli.BoolTFlag{
				Name:   config.NvdDbUpdateParam,
				EnvVar: config.NvdDbUpdateEnv,
				Usage:  "Automatically update the nvd-mirror database for every run.",
			},
			&cli.BoolTFlag{
				Name:   config.UseOmnibusPathsParam,
				EnvVar: config.UseOmnibusPathsEnv,
				Usage:  "Set path to software definitions in report file.",
			},
		},
		Convert: convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
