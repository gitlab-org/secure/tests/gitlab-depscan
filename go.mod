module gitlab-depscan

go 1.13

require (
	github.com/logrusorgru/aurora v0.0.0-20200102142835-e9ef32dff381
	github.com/sirupsen/logrus v1.4.2
	github.com/spiegel-im-spiegel/go-cvss v0.2.1
	github.com/umisama/go-cvss v0.0.0-20150430082624-a4ad666ead9b
	github.com/urfave/cli v1.22.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.10.3
	gitlab.com/gitlab-org/security-products/cwe-info-go v1.0.1
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
