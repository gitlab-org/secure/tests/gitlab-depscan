package main

import (
	"bytes"
	"fmt"
	"gitlab-depscan/config"
	mirror "gitlab-depscan/nvdmirror"
	"gitlab-depscan/omnibus"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// DENYLIST might be propagated here by the `match` function if it finds a matching denylist file,
	// so try that first
	denylist := os.Getenv(config.DenyListEnv)
	if f := c.String(config.DenyListParam); f != "" {
		// if a user-defined DENYLIST value has been provided, use that instead of any matched files
		denylist = f
	}

	// MANIFEST might be propagated here by the `match` function if it finds a matching manifest file,
	// so try that first
	manifest := os.Getenv(config.ManifestEnv)
	if f := c.String(config.ManifestParam); f != "" {
		// if a user-defined MANIFEST value has been provided, use that instead of any matched files
		manifest = f
	}

	log.Infof("Using %s as denylist", denylist)
	log.Infof("Using %s as manifest file", manifest)

	var cvesToIgnore = []string{}

	if _, err := os.Stat(denylist); os.IsNotExist(err) {
		log.Warnf("Denylist file with path '%s' does not exist, skipping", denylist)
	} else {
		cvesToIgnore, err = omnibus.ParseDenylist(denylist)
		if err != nil {
			log.Warnf("Could not read denylist %s, skipping", denylist)
		}
	}

	log.Infof("CVEs to ignore: '%s'", strings.Join(cvesToIgnore, "', '"))

	dependencies, err := omnibus.ParseVersionManifest(manifest)
	if err != nil {
		log.Errorf("Could not read manifest file %s", path)
		return nil, err
	}

	request, err := mirror.GenerateNVDMirrorRequest(dependencies, cvesToIgnore)
	if err != nil {
		log.Errorf("Could not generate npm mirror request")
		return nil, err
	}

	file, err := ioutil.TempFile("/tmp", "tmpfile")
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	file.WriteString(request)
	file.Close()
	defer os.Remove(file.Name())

	if c.Bool(config.NvdDbUpdateParam) {
		log.Infof("Updating nvd-mirror-database")
		cmd := exec.Command("update-db.sh")
		cmd.Env = os.Environ()
		cmdOutput, err := cmd.CombinedOutput()
		if err != nil {
			return nil, fmt.Errorf("An error occurred while attempting to update the nvd-mirror-database. Error: %w. Command Output: %s", err, string(cmdOutput))
		}
		log.Debugf("Successfully updated nvd-mirror-database. Command output: \n%s", cmdOutput)
	} else {
		log.Infof("Not updating nvd-mirror-database because %s=false or --%s=false", config.NvdDbUpdateEnv, config.NvdDbUpdateParam)
	}

	log.Infof("Running nvd-mirror")

	fpath, _ := filepath.Abs(file.Name())

	cmd := exec.Command("/nvd-mirror.sh", fpath)
	cmd.Dir = "."
	cmd.Env = os.Environ()

	cmdOutput, err := cmd.Output()
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	log.Debugf("nvdmirror output: %s", string(cmdOutput))

	return ioutil.NopCloser(bytes.NewReader(cmdOutput)), err
}
