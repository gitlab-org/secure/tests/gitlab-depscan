#!/bin/sh
REPORT_FILE="${CI_PROJECT_DIR}/gl-dependency-scanning-report.json"
PLAINTEXT_REPORT="${CI_PROJECT_DIR}/dependency_report.txt"
VERSION_MANIFEST="$1"
DENYLIST="$2"

USAGE="gitlab-depscan.sh <manifest> <denylist>"

[ -f "$VERSION_MANIFEST" ] || {
  echo "version manifest not provided"
  echo "$USAGE"
  exit 1
}

/analyzer run --manifest "$VERSION_MANIFEST" --denylist "$DENYLIST"

[ -f "$REPORT_FILE" ] || {
  echo "$REPORT_FILE does not exist"
  exit 1
}

# generates a human-readable report
jq -r '.vulnerabilities | map([.location.dependency.package.name, .cve, .message, .severity] | join(",")) | join("\n")' "$REPORT_FILE" >"$PLAINTEXT_REPORT"

# count the number of vulnerabilities
RES="$(jq '.vulnerabilities | length' "$REPORT_FILE")"

[ "$RES" = 0 ] && exit 0

# if vulnerability count is >0, return error
exit 1
