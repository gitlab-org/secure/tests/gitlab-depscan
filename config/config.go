package config

const (
	// UseOmnibusEnv is an environment variable that can be used to
	// enable/disable setting locations to software definitions
	UseOmnibusPathsEnv = "DEPSCAN_USE_OMNIBUS_PATHS"
	// UseOmnibusPath is an command line parameter that can be used to
	// enable/disable setting locations to software definitions
	UseOmnibusPathsParam = "use-omnibus-paths"
	// DenyListEnv is an environment variable that can used to set path to the denylist file
	DenyListEnv = "DENYLIST"
	// DenyListParam is a command line parameter that can used to set path to the denylist file
	DenyListParam = "denylist"
	// ManifestEnv is an environment variable that can used to set path to the manifest file
	ManifestEnv = "MANIFEST"
	// ManifestParam is a command line parameter that can used to set path to the manifest file
	ManifestParam = "manifest"
	// NvdDbUpdateEnv is an environment variable that can used to enable/disable automated database updates for nvd-mirror
	NvdDbUpdateEnv = "NVD_DB_UPDATE"
	// NvdDbUpdateParam is a command line parameter that can used to enable/disable automated database updates for nvd-mirror
	NvdDbUpdateParam = "nvd-db-update"
	// AnalyzerName is the name of gitlab-depscan as it appears in the output log
	AnalyzerName = "gitlab-depscan"
)
