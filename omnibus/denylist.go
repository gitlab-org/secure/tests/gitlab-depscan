package omnibus

import (
	"bufio"
	"io/ioutil"
	"strings"
)

// ParseDenylist parses a denylist that contains CVEs to be ingored
func ParseDenylist(path string) ([]string, error) {
	denylistData, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return ProcessDenylistConent(string(denylistData)), nil
}

// ProcessDenylistConent processes the content of the denylist
func ProcessDenylistConent(content string) []string {
	cves := []string{}
	scanner := bufio.NewScanner(strings.NewReader(content))
	for scanner.Scan() {
		trimmed := strings.TrimSpace(scanner.Text())
		if trimmed == "" {
			continue
		}
		if strings.HasPrefix(trimmed, "#") {
			continue
		}
		cves = append(cves, trimmed)
	}
	return cves
}
