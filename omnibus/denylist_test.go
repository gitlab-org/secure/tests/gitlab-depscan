package omnibus

import (
	"reflect"
	"testing"
)

func TestParseDenyist(t *testing.T) {
	denylist := `
# Here is a comment
CVE-2019-15548

# Here is another comment
CVE-2019-17594
`
	expect := []string{"CVE-2019-15548", "CVE-2019-17594"}
	parsed := ProcessDenylistConent(denylist)
	if !reflect.DeepEqual(ProcessDenylistConent(denylist), []string{"CVE-2019-15548", "CVE-2019-17594"}) {
		t.Fatalf("got:%v, expected:%v", parsed, expect)
	}
}
