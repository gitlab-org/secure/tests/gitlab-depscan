# GitLab Depscan changelog

## v2.2.0
- Support for CVSS 3.1 (!23)
- Add proper file links to report (!24)

## v2.1.0
- Incorporate `vendor` field when searching for vulnerabilities in dependencies.

## v2.0.0
- Switch to `display_version` field in `version-manifest.json` to search for vulnerabilities.

## v1.1.0
- Added the `NVD_DB_UPDATE` environment variable to enable/disable automated nvd-mirror database updates (!16)

## v1.0.1
- Fix bug preventing execution of `nvd-mirror.sh` script by using absolute path (!15)

## v1.0.0
- Initial release
