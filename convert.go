package main

import (
	"fmt"
	"gitlab-depscan/config"
	"gitlab-depscan/cvss"
	"gitlab-depscan/logutils"
	"gitlab-depscan/nvdmirror"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strconv"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	cweinfo "gitlab.com/gitlab-org/security-products/cwe-info-go"
)

const (
	scannerID   = "gitlab-depscan"
	scannerName = "GitLab Depscan"
)

var scanner = issue.Scanner{
	ID:   scannerID,
	Name: scannerName,
}

type vulnerability struct {
	cve, packageName, URL string
}

type vulnerabilitiesWithSeverity map[issue.SeverityLevel][]vulnerability

func addVulnerabilityWithSeverity(vuln issue.DependencyScanningVulnerability, vulns vulnerabilitiesWithSeverity) {
	vulnerability := vulnerability{
		cve:         vuln.Identifiers[0].Value,
		packageName: vuln.Location.Dependency.Package.Name,
		URL:         vuln.Links[0].URL,
	}

	severityVal := vuln.Severity
	vulns[severityVal] = append(vulns[severityVal], vulnerability)
}

func outputVulnerabilities(vulns vulnerabilitiesWithSeverity) {
	severityLevels := []issue.SeverityLevel{
		issue.SeverityLevelCritical,
		issue.SeverityLevelHigh,
		issue.SeverityLevelMedium,
		issue.SeverityLevelLow,
		issue.SeverityLevelInfo,
		issue.SeverityLevelUnknown,
	}

	for _, severityLevel := range severityLevels {
		vulnerabilitiesForSeverityLevel := vulns[severityLevel]

		for _, vulnerability := range vulnerabilitiesForSeverityLevel {
			msg := fmt.Sprintf("Found %s for '%s' with severity '%s': %s",
				vulnerability.cve,
				vulnerability.packageName,
				severityLevel.String(),
				vulnerability.URL,
			)
			logutils.LogVulnerability(msg, severityLevel)
		}
	}
}

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {

	log.Infof("Generating report")

	location := filepath.Join(prependPath, os.Getenv(config.ManifestEnv))

	buf, err := ioutil.ReadAll(reader)
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	nvdReport, err := nvdmirror.ParseNvdMirrorReport(string(buf))
	if err != nil {
		log.Errorf(err.Error())
		return nil, err
	}

	issues := []issue.Issue{}

	vulnerabilitiesWithSeverity := vulnerabilitiesWithSeverity{}

	for vendor, vendorDetails := range nvdReport {
		for product, productDetails := range vendorDetails {
			for version, versionDetails := range productDetails {
				for _, cve := range versionDetails {
					title := ""
					cwe, err := cweinfo.GetCweInfo(strconv.Itoa(cve.Cwe))
					if err == nil {
						title = cwe.Title
					}
					severity := cvss.Severity(cve.Cvss)
					link := fmt.Sprintf("https://nvd.nist.gov/vuln/detail/%s", cve.ID)

					// Instead of pointing to version-manifest.json file, point
					// to the software definition file. Useful in
					// omnibus-gitlab's vulnerability report page.
					// This variable is expected to be set only in
					// omnibus-gitlab project.
					if os.Getenv(config.UseOmnibusPathsEnv) == "true" {
						location = fmt.Sprintf("config/software/%s.rb", url.QueryEscape(product))
					}

					vuln := issue.DependencyScanningVulnerability{
						Issue: issue.Issue{
							Category: issue.CategoryDependencyScanning,
							Scanner:  scanner,
							Name:     title, // no package name
							Severity: severity,
							Location: issue.Location{
								File: location,
								Dependency: &issue.Dependency{
									Package: issue.Package{
										Name: filepath.Join(vendor, product),
									},
									Version: version,
								},
							},
							Identifiers: []issue.Identifier{issue.CVEIdentifier(cve.ID)},
							Links:       issue.NewLinks(link),
						},
					}
					issues = append(issues, vuln.ToIssue())
					addVulnerabilityWithSeverity(vuln, vulnerabilitiesWithSeverity)
				}
			}
		}
	}

	outputVulnerabilities(vulnerabilitiesWithSeverity)

	report := issue.NewReport()
	report.Vulnerabilities = issues

	return &report, nil
}
