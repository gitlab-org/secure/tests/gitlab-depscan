FROM golang:1.14.2-alpine3.11 AS builder
COPY . /build
WORKDIR /build
RUN go build -o /analyzer

FROM registry.gitlab.com/gitlab-org/secure/vulnerability-research/advisories/nvd-mirror:1.0.2
RUN apk add --no-cache jq go
COPY gitlab-depscan.sh nvd-mirror.sh /
COPY --from=builder /analyzer /
WORKDIR "/"
ENTRYPOINT []
CMD ["/analyzer", "run"]
