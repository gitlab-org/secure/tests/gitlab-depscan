package nvdmirror

import (
	"gitlab-depscan/omnibus"
	"reflect"
	"testing"
)

func TestNvdMirrorRequest(t *testing.T) {
	manifestString := `
{
		"manifest_format": 2,
		"software": {
			"ncurses": {
				"locked_version": "5.9",
					"locked_source": {
					"md5": "8cb9c412e5f2d96bc6f459aa8c6282a1",
						"url": "http://ftp.gnu.org/gnu/ncurses/ncurses-5.9.tar.gz"
				},
				"display_version": "5.9",
				"source_type": "url",
					"described_version": "5.9",
					"license": "MIT"
			}
	},
	"build_version": "12.2.4+rnightly.125492.5032ed10",
	"build_git_revision": "5032ed10c547382d5ae19aea89d9f15dad202003",
	"license": "MIT"
}
`
	depVersions, err := omnibus.ProcessVersionManifestContent(manifestString)
	if err != nil {
		t.Fatalf(err.Error())
	}

	request, err := GenerateNVDMirrorRequest(depVersions, []string{"CVE-123"})
	if err != nil {
		t.Fatalf("Npm Mirror Request generation failed")
	}

	expect := `{"query_items":[{"product":"ncurses","vendor":"","version":"5.9"}],"cve_denylist":["CVE-123"]}`
	if !reflect.DeepEqual(request, expect) {
		t.Fatalf("got:%v,expected:%v", request, expect)
	}
}

func TestNvdMirrorRequestWithVendor(t *testing.T) {
	manifestString := `
{
		"manifest_format": 2,
		"software": {
			"openssl": {
				  "locked_version": "e2e09d9fba1187f8d6aafaa34d4172f56f1ffb72",
				  "locked_source": {
					"git": "git@dev.gitlab.org:omnibus-mirror/openssl.git"
				  },
				  "source_type": "git",
				  "described_version": "OpenSSL_1_1_1g",
				  "display_version": "1.1.1g",
				  "vendor": "openssl",
				  "license": "OpenSSL"
				}
		}
}
`
	depVersions, err := omnibus.ProcessVersionManifestContent(manifestString)
	if err != nil {
		t.Fatalf(err.Error())
	}

	request, err := GenerateNVDMirrorRequest(depVersions, []string{"CVE-123"})

	if err != nil {
		t.Fatalf("NVD Mirror Request generation failed")
	}

	expect := `{"query_items":[{"product":"openssl","vendor":"openssl","version":"1.1.1g"}],"cve_denylist":["CVE-123"]}`
	if !reflect.DeepEqual(request, expect) {
		t.Fatalf("got:%v,expected:%v", request, expect)
	}
}
