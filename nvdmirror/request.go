package nvdmirror

import (
	"encoding/json"
	"gitlab-depscan/omnibus"
)

type queryItem struct {
	Product string `json:"product"`
	Vendor  string `json:"vendor"`
	Version string `json:"version"`
}

// Request represents a query for nvdmirror
type Request struct {
	QueryItems  []queryItem `json:"query_items"`
	CveDenylist []string    `json:"cve_denylist"`
}

// GenerateNVDMirrorRequest generates a query for nvdmirror
func GenerateNVDMirrorRequest(usedDependencies []omnibus.UsedDependency, cveDenylist []string) (string, error) {
	jsonVersionConstraint := []queryItem{}

	for _, dep := range usedDependencies {
		jsonVersionConstraint = append(jsonVersionConstraint, queryItem{Product: dep.Name, Vendor: dep.Vendor, Version: dep.Version})
	}

	jsonQuery := Request{}
	jsonQuery.CveDenylist = cveDenylist
	jsonQuery.QueryItems = jsonVersionConstraint

	bytes, err := json.Marshal(jsonQuery)
	if err != nil {
		return "", err
	}

	return string(bytes), nil
}
