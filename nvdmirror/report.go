package nvdmirror

import (
	"encoding/json"
)

// Report represents a report returned by nvdmirror
type Report map[string]Product2Version

// Product2Version maps product to version info
type Product2Version map[string]Version2Cve

// Version2Cve ka-s version to Cwe info
type Version2Cve map[string][]Cve

// Cve represents a single CVE entry
type Cve struct {
	ID   string `json:"cve"`
	Cwe  int    `json:"cwe"`
	Cvss string `json:"cvss"`
}

// ParseNvdMirrorReport parses the JSON report from nvdmirror
func ParseNvdMirrorReport(nvdMirrorReport string) (Report, error) {
	var mirrorReport Report
	err := json.Unmarshal([]byte(nvdMirrorReport), &mirrorReport)
	if err != nil {
		return nil, err
	}
	return mirrorReport, nil
}
