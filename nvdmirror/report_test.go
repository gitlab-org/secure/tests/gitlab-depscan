package nvdmirror

import (
	"reflect"
	"testing"
)

func TestParseVulnerableVersions(t *testing.T) {
	mirrorReport := `
{
  "nginx": {
    "nginx": {
      "1.2": [
        {
          "cve": "CVE-2016-0742",
          "cwe": 0,
          "cvss": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:L"
        },
        {
          "cve": "CVE-2016-0746",
          "cwe": 0,
          "cvss": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L"
        },
        {
          "cve": "CVE-2016-0747",
          "cwe": 399,
          "cvss": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:L"
        },
        {
          "cve": "CVE-2016-1247",
          "cwe": 59,
          "cvss": "CVSS:3.0/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H"
        }
      ]
    }
  }
}
`
	for _, tt := range []struct {
		in             string
		expectedResult Report
	}{
		{
			mirrorReport,
			Report{
				"nginx": Product2Version{
					"nginx": Version2Cve{
						"1.2": []Cve{
							{"CVE-2016-0742", 0, "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:L"},
							{"CVE-2016-0746", 0, "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L"},
							{"CVE-2016-0747", 399, "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:L"},
							{"CVE-2016-1247", 59, "CVSS:3.0/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H"},
						},
					},
				},
			},
		},
	} {
		parsedReport, err := ParseNvdMirrorReport(tt.in)

		if err != nil {
			t.Fatalf("nvd mirror report not parseable")
		}

		if !reflect.DeepEqual(tt.expectedResult, parsedReport) {
			t.Fatalf("got:%v, expected:%v", parsedReport, tt.expectedResult)
		}
	}
}
